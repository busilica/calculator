package com.example.daca.calculator.presenter;

import com.example.daca.calculator.model.Calculation;
import com.example.daca.calculator.CalculatorContract;

/**
 * Created by Daca on 26.02.2017..
 */

public class CalculatorPresenter implements CalculatorContract.ForwardDisplayInteractionToPresenter,
                                            CalculatorContract.ForwardInputInteractionToPresenter,
        Calculation.CalculationResult {

    private CalculatorContract.PublishToView publishResult;
    private Calculation calc;

                                //in reality this will be an object of DisplayFragment, but Presenter doesn't know that
    public CalculatorPresenter(CalculatorContract.PublishToView publishResult) {
        this.publishResult = publishResult;      //we are not passing any View classes
        calc = new Calculation();
        calc.setCalculationResultListener(this);
    }                                            //Presenter doesn't know about them he just knows -
                                                 //- about interface through which it will communicate
    @Override
    public void onDeleteShortClick() {
        calc.deleteCharacter();
    }

    @Override
    public void onDeleteLongClick() {
        calc.deleteExpression();
    }

    @Override
    public void onNumberClick(int number) {
        calc.appendNumber(Integer.toString(number));
    }

    @Override
    public void onDecimalClick() {
        calc.appendDecimal();
    }

    @Override
    public void onEvaluateClick() {
        calc.performEvaluation();
    }

    @Override
    public void onOperatorClick(String operator) {
        calc.appendOperator(operator);
    }

    @Override
    public void onExpressionChanged(String result, boolean successful) {
        if (successful){
            publishResult.showResult(result);
        }else {
            publishResult.showToastMessage(result);
        }
    }
}
