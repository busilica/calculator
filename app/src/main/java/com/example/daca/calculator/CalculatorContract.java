package com.example.daca.calculator;

/**
 * Created by Daca on 26.02.2017..
 */
//purpose of Contract is how different components are going to talk to each other
public interface CalculatorContract {

    //Our View handles these methods
    interface PublishToView{
        void showResult(String result);         //if valid show result
        void showToastMessage(String message);  //if not valid show Toast message
    }

    //passes click events from our View (DisplayFragment) to the Presenter
    interface ForwardDisplayInteractionToPresenter{
        void onDeleteShortClick();  //we are not passing anything we just need to let Presenter know -
        void onDeleteLongClick();   //-that something (click, long click) has happened
    }

    //passes click events from our View (InputFragment) to the Presenter
    interface ForwardInputInteractionToPresenter{
        void onNumberClick(int number);
        void onDecimalClick();
        void onEvaluateClick();
        void onOperatorClick(String operator);
    }

}
