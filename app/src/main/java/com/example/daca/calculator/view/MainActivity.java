package com.example.daca.calculator.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.daca.calculator.DisplayFragment;
import com.example.daca.calculator.InputFragment;
import com.example.daca.calculator.R;
import com.example.daca.calculator.presenter.CalculatorPresenter;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DisplayFragment displayFragment = (DisplayFragment) getSupportFragmentManager()
                .findFragmentById(R.id.frag_display);

        InputFragment inputFragment = (InputFragment) getSupportFragmentManager()
                .findFragmentById(R.id.frag_input);

        CalculatorPresenter presenter = new CalculatorPresenter(displayFragment); //displayFragment implements required interface
        displayFragment.setPresenter(presenter);
        inputFragment.setPresenter(presenter);
    }
}
